//#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string.h>
#include <string>
#include <Windows.h>
#include <stdio.h>
#include "sudokugame.h"
using namespace std;

bool Solver(char SudokuBoard[9][9], int N);
bool ForRowCol(char SudokuBoard[9][9], int Row, int Col, char num);
bool ForBox(char SudokuBoard[9][9], int row, int col, char num);
int NumEmpty(char SudokuBoard[9][9]);



int main()
{
	char SudokuBoard[9][9];
	char SudokuRecord[9][9];

	Start(SudokuBoard, SudokuRecord);


	return 0;
}


//start a game
void Start(char SudokuBoard[9][9], char SudokuRecord[9][9])
{
	cout << "cmd     - Description \n"
		<< "******************************************* \n"
		<< "help    - Help \n"
		<< "load    - Load game \n"
		<< "new     - Start a new game \n"
		<< "quit    - Quit game \n"
		<< "Please type a command: \n"
		<< ">" << endl;

	char Command[5];
	cin >> Command;

	char HELP[5] = "HELP";
	char LOAD[5] = "LOAD";
	char NEW[5] = "NEW";
	char QUIT[5] = "QUIT";

	if (_stricmp(Command, HELP) == 0) {
		Start(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command, LOAD) == 0) {
		LoadGame(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command, NEW) == 0) {
		New(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command, QUIT) == 0) {
		exit(1);
	}
	else {
		cout << "ERROR: Invalid Commnd" << endl;
		Start(SudokuBoard, SudokuRecord);
	}

	return;

}

//randomly provide a hint to user
void Hint(char SudokuBoard[9][9], char SudokuRecord[9][9])
{
	int i = 0;
	int num = 1;
	char ROW[30] = "ABCDEFGHI"; //?

	while (i < 1)
	{
		int r = (rand() % 9);
		int c = (rand() % 9);
		if (SudokuBoard[r][c] = ' ') {
			while (!(ForRowCol(SudokuBoard, r, c, num) == false && ForBox(SudokuBoard, r, c, num) == false))
			{
				num++;
			}
			cout << ROW[r] << c << "=" << num << endl;
			i++;
		}
	}
	Command(SudokuBoard, SudokuRecord);
}

//randomly generate a sudoku board 
void New(char SudokuBoard[9][9], char SudokuRecord[9][9])
{
	int n = 0;
	int N;
	int temp;
	cout << "Please input the number of fixed cell" << endl;
	cin >> n;
	if (n < 1 || n > 81) {
		cout << "Invalid number \n" << endl;
		Start(SudokuBoard, SudokuRecord);
	}

	//initialize record table
	for (int row = 0; row < 9; row++) {
		for (int col = 0; col < 9; col++) {
			SudokuBoard[row][col] = ' ';
			SudokuRecord[row][col] = 2;
		}
	}
	//generate random row, col, value
	int r = rand() % 9;
	int c = rand() % 9;
	int v = (1.0 + rand() % 9);
	v = v + '0';
	SudokuBoard[r][c] = v;
	//solve new board
	N = NumEmpty(SudokuBoard);
	Solver(SudokuBoard, N);



	temp = 81 - n;
	while (temp>0)
	{
		int r = (rand() % 9);
		int c = (rand() % 9);
		if (SudokuBoard[r][c] != ' ')
		{
			SudokuBoard[r][c] = ' ';
			SudokuRecord[r][c] = 0;
			temp -= 1;
		}

	}
	Draw(SudokuBoard, SudokuRecord);
	Help(SudokuBoard, SudokuRecord);

}



//input a move on sudoku board
void Move(char SudokuBoard[9][9], char SudokuRecord[9][9])
{
	char M[100];

	cout << "Please input a move (eg. A1=1)" << endl;

	cin >> skipws;
	cin >> M;

	if (strlen(M) != 4)
	{
		cout << "Input doesn't contain 4 characters! " << endl;
	}
	else
	{
		M[0] = toupper(M[0]);
		cout << M[0] << endl;
		//check input
		if ((M[0] >= 'A'&&M[0] <= 'I') && (M[1] >= '1'&&M[1] <= '9') && (M[2] = '=') && (M[3] >= '1'&&M[3] <= '9'))
		{
			if (SudokuRecord[M[0] - 65][M[1] - 49] == 0)
			{
				//give hint if user provide a wrong input
				if (ForRowCol(SudokuBoard, M[0] - 65, M[1] - 49, M[3]) == false && ForBox(SudokuBoard, M[0] - 65, M[1] - 1, M[3]) == false) {
					SudokuBoard[M[0] - 65][M[1] - 49] = M[3];
					SudokuRecord[M[0] - 65][M[1] - 49] = 1;
					system("cls");
					Draw(SudokuBoard, SudokuRecord);
				}
				else {
					cout << "Wrong input: there is a repeated number in col/row/sqaure" << endl;
				}
			}
			else if (SudokuRecord[M[0] - 65][M[1] - 49] == 2)
			{
				cout << M[0] << M[1] << " " << "is fixed" << endl;

			}
			else if (SudokuRecord[M[0] - 65][M[1] - 49] == 1) {
				cout << M[0] << M[1] << " " << "already has a guess!" << endl;
			}
		}
		else
		{
			cout << "Invalid format" << endl;
		}
	}
	Command(SudokuBoard, SudokuRecord);

}

//Undo a cell in sudoku board
void Undo(char SudokuBoard[9][9], char SudokuRecord[9][9])
{
	char U[100];

	cout << "Please input a cell coordinate (eg, A1)" << endl;
	cin >> U;

	if (strlen(U) != 2)
	{
		cout << "Input doesn't contain 2 characters! " << endl;
	}
	else{
		U[0] = toupper(U[0]);
		if ((U[0] >= 'A'&&U[0] <= 'I') && (U[1] >= '1'&&U[1] <= '9'))
		{
			if (SudokuRecord[U[0] - 65][U[1] - 49] == 0)
			{

				cout << U[0] << U[1] << " is empty" << endl;
			}
			else if (SudokuRecord[U[0] - 65][U[1] - 49] == 2)
			{
				cout << U[0] << U[1] << " " << "is fixed" << endl;

			}
			else if (SudokuRecord[U[0] - 65][U[1] - 49] == 1) {
				SudokuBoard[U[0] - 65][U[1] - 49] = ' ';
				SudokuRecord[U[0] - 65][U[1] - 49] = 0;
				system("cls");
				Draw(SudokuBoard, SudokuRecord);
			}
		}
		else
		{
			cout << "Invalid format" << endl;
		}
	}
	Command(SudokuBoard, SudokuRecord);
}

//out put the function of commands
void Help(char SudokuBoard[9][9], char SudokuRecord[9][9])
{
	system("cls");
	Draw(SudokuBoard, SudokuRecord);
	cout << "cmd     - Description \n"
		<< "******************************************* \n"
		<< "clear       - Refresh \n"
		<< "help        - Help \n"
		<< "hint        - Hint \n"
		<< "move        - Move \n"
		<< "quit        - Quit game \n"
		<< "save        - Save \n"
		<< "show        - Show solution \n"
		<< "undo        - UndoMove \n"
		<< "back        - Back to main menu \n" << endl;
	Command(SudokuBoard, SudokuRecord);
}

void Command(char SudokuBoard[9][9], char SudokuRecord[9][9])
{
	cout << "Please type a command(Type help to see commands details) : \n"
		<< ">" << endl;
	int N = 0;
	char Command1[6];
	cin >> Command1;
	char HELP[5] = "HELP";
	char HINT[5] = "HINT";
	char MOVE[5] = "MOVE";
	char QUIT[5] = "QUIT";
	char SAVE[5] = "SAVE";
	char SHOW[5] = "SHOW";
	char UNDO[5] = "UNDO";
	char BACK[5] = "BACK";
	char CLEAR[6] = "CLEAR";


	if (_stricmp(Command1, HELP) == 0) {
		Help(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command1, MOVE) == 0) {
		Move(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command1, QUIT) == 0) {
		exit(1);
	}
	else if (_stricmp(Command1, SAVE) == 0) {
		Save(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command1, SHOW) == 0) {
		N = NumEmpty(SudokuBoard);
		Solver(SudokuBoard, N);
		Draw(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command1, UNDO) == 0) {
		Undo(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command1, HINT) == 0) {
		Hint(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command1, BACK) == 0) {
		system("cls");
		Start(SudokuBoard, SudokuRecord);
	}
	else if (_stricmp(Command1, CLEAR) == 0) {
		system("cls");
		Draw(SudokuBoard, SudokuRecord);
	}
	else {
		cout << "ERROR: Invalid Command" << endl;
	}

	Command(SudokuBoard, SudokuRecord);
}


//Calculate the number of empty cell
int NumEmpty(char SudokuBoard[9][9])
{
	int i = 0;
	for (int Row = 0; Row < 9; Row++) {
		for (int Col = 0; Col < 9; Col++) {
			if (SudokuBoard[Row][Col] == ' ') {
				i++;
			}
		}
	}
	return i;
}

//Save board to input file path
void Save(char SudokuBoard[9][9], char SudokuRecord[9][9])
{
	int i = 0;
	string c;
	char Path[256];

	cout << "Please input a file path\n" << ">" << endl;
	cin >> Path;
	ofstream fout;
	fout.open(Path);
	if (fout.fail()) {
		cout << "fail to open file\n" << "Please type help to see commands details \n" << endl;
	}


	for (int Row = 0; Row < 9; Row++) {
		for (int Col = 0; Col < 9; Col++)
		{

			if (SudokuRecord[Row][Col] == 1)
			{

				//Record user's guess
				c += 'g';
				c += SudokuBoard[Row][Col];
				c += ',';
			}
			c += SudokuBoard[Row][Col];
			c += ',';
		}
	}
	fout << c;

	fout.close();


}




// Load game from input file path
void LoadGame(char SudokuBoard[9][9], char SudokuRecord[9][9]) {
	// buffer
	char c[256];
	char Path[256];
	int i = 0;

	//file input

	cout << "Please input a file path\n" << ">" << endl;
	cin >> Path;
	ifstream fin;
	fin.open(Path);
	if (fin.fail())
	{
		cout << "Error opening SudokuBoard" << endl;
		Start(SudokuBoard, SudokuRecord);
	}
	fin >> noskipws;
	fin.getline(c, 256);

	for (int Row = 0; Row < 9; Row++)
	for (int Col = 0; Col < 9; Col++)
	{

		SudokuBoard[Row][Col] = c[i];
		//skip when encounter comma
		if (SudokuBoard[Row][Col] == ',')
		{
			i++;
			SudokuBoard[Row][Col] = c[i];

		}

		//check if 
		if (SudokuBoard[Row][Col] <= '9' && SudokuBoard[Row][Col] >= '1') {
			SudokuRecord[Row][Col] = 2;
		}
		else if (SudokuBoard[Row][Col] == 'g')
		{
			i++;
			//Record user's guess
			SudokuRecord[Row][Col] = 1;
			SudokuBoard[Row][Col] = c[i];
		}
		else
		{
			SudokuRecord[Row][Col] = 0;
		}

		if (!(((SudokuBoard[Row][Col] <= '9' && SudokuBoard[Row][Col] >= '1') || SudokuBoard[Row][Col] == ' '))) {
			system("cls");
			cout << "Invalid Board! Board should be saved as the format of :" << endl;
			cout << "9, , ,1, ,8,g3, ,.......(letter 'g' means user's guess)" << endl;
			break;
		}


		i++;
	}

	fin.close();

	Draw(SudokuBoard, SudokuRecord);
	Help(SudokuBoard, SudokuRecord);

}


//Draw sudoku board
void Draw(char SudokuBoard[9][9], char SudokuRecord[9][9]) {

	system("CLS");
	char ROW[30] = "ABCDEFGHI";
	int i = 0;
	//Column header
	cout << "  | 1  2  3 | 4  5  6 | 7  8  9" << endl;



	for (int Row = 0; Row < 9; Row++)
	{

		if (Row % 3 == 0) {
			cout << "--+---------+---------+---------" << endl;

		}
		cout << ROW[Row] << " ";

		for (int Col = 0; Col < 9; Col++)
		{
			if (Col % 3 == 0) {
				cout << "|";
			}
			int j = 0;
			if (SudokuRecord[Row][Col] == 1)
			{
				//change the color of user's guess
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED);
				j = 1;
			}

			cout << " " << SudokuBoard[Row][Col] << " ";
			//recover to oringial color 
			if (j == 1) {
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
			}

		}

		cout << endl;


	}
}


//solve sudoku board
bool Solver(char SudokuBoard[9][9], int N) {
	if (N == 0) {
		return true;// Problem solved
	}

	//Record coordinate
	int x = 0;
	int y = 0;
	int IsEmpty = 0;

	//get next empty cell
	for (int Row = 0; Row < 9; Row++) {
		for (int Col = 0; Col < 9; Col++) {
			if (SudokuBoard[Row][Col] == ' ') {
				x = Row;
				y = Col;
				IsEmpty = 1;
				break;
			}

		}
		if (IsEmpty == 1) {
			break;
		}
	}

	for (int i = 1; i < 10; i++) {
		char num = i + '0';
		if (ForRowCol(SudokuBoard, x, y, num) == false && ForBox(SudokuBoard, x, y, num) == false) {

			SudokuBoard[x][y] = num;

			if (Solver(SudokuBoard, N - 1)) {
				return true;
			}
			else {
				SudokuBoard[x][y] = ' ';

			}
		}
	}

	return false;

}



/* Returns a boolean which indicates whether any assigned entry
in the specified row matches the given number. */
bool ForRowCol(char SudokuBoard[9][9], int Row, int Col, char num)
{
	for (int l = 0; l < 9; l++)
	{
		if (SudokuBoard[Row][l] == num || SudokuBoard[l][Col] == num)
		{
			return true;
		}
	}
	return false;

}

/* Returns a boolean which indicates whether any assigned entry
within the specified 3x3 box matches the given number. */
bool ForBox(char SudokuBoard[9][9], int row, int col, char num)
{
	for (int startrow = 0; startrow < 3; startrow++)
	{
		for (int startcol = 0; startcol < 3; startcol++)
		{
			if (SudokuBoard[row - row % 3 + startrow][col - col % 3 + startcol] == num)
			{
				return true;
			}
		}
	}
	return false;
}