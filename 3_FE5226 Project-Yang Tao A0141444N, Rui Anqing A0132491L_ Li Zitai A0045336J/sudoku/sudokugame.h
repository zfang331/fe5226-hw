#ifndef SUDOKUGAME_H_INCLUDED
#define SUDOKUGAME_H_INCLUDED
#endif // SUDOKUGAME_H_INCLUDED
// Below is the defination of the statemachine
void LoadGame(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void Draw(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void Save(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void Start(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void New(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void Move(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void Help(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void Command(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void Undo(char SudokuBoard[9][9], char SudokuRecord[9][9]);
void Hint(char SudokuBoard[9][9], char SudokuRecord[9][9]);

